double clicking run_for_all_files.bat will execute the algorithm for all files stored in the bencmarks folder. output files are generated in the output folder.

this is a batch file, and won't work directly on non windows systems. the algorithm itself should be platform independent, but was not tested on a non windows system.
@echo off
IF EXIST "do_copy" copy ..\cmake-build-debug\main.exe main.exe
IF EXIST "..\outputs\compare_results\all_benchmarks_compare_results.txt" del "..\outputs\compare_results\all_benchmarks_compare_results.txt"
for /R "..\benchmarks\" %%i in (*) do  (
    echo running algorithm for %%~nxi 
    main.exe -c -C "all_benchmarks_compare_results.txt" -a -t -f %%i
    echo.
    )
pause
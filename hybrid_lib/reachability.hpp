#pragma once

#include "types.hpp"
namespace hybrid
{
    struct mealy;

    mealy reachable_submachine(const mealy &in, state start);
}
no extra filter

all results

cases that passed the filter: 300
cases where length and reset changes did not match up: 8
cases where length improved by at least 5%: 131
cases where length improved by at least 10%: 121
cases where length improved by at least 30%: 65
cases where length worsened by at least 5%: 88
cases where length worsened by at least 10%: 72
cases where length worsened by at least 30%: 39
cases where length worsened by at least 50%: 12
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 138
cases where the resets improved by at least 10%: 135
cases where the resets improved by at least 30%: 105
cases where the resets worsened by at least 5%: 84
cases where the resets worsened by at least 10%: 61
cases where the resets worsened by at least 30%: 29
cases where the resets worsened by at least 50%: 7
cases where the resets worsened by at least 100%: 4


50 or more states

cases that passed the filter: 75
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 46
cases where length improved by at least 10%: 46
cases where length improved by at least 30%: 16
cases where length worsened by at least 5%: 13
cases where length worsened by at least 10%: 13
cases where length worsened by at least 30%: 13
cases where length worsened by at least 50%: 5
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 46
cases where the resets improved by at least 10%: 46
cases where the resets improved by at least 30%: 45
cases where the resets worsened by at least 5%: 13
cases where the resets worsened by at least 10%: 13
cases where the resets worsened by at least 30%: 13
cases where the resets worsened by at least 50%: 5
cases where the resets worsened by at least 100%: 4


200 or more states

cases that passed the filter: 39
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 29
cases where length improved by at least 10%: 29
cases where length improved by at least 30%: 7
cases where length worsened by at least 5%: 7
cases where length worsened by at least 10%: 7
cases where length worsened by at least 30%: 7
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 29
cases where the resets improved by at least 10%: 29
cases where the resets improved by at least 30%: 29
cases where the resets worsened by at least 5%: 7
cases where the resets worsened by at least 10%: 7
cases where the resets worsened by at least 30%: 7
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


10 or more inputs

cases that passed the filter: 193
cases where length and reset changes did not match up: 1
cases where length improved by at least 5%: 99
cases where length improved by at least 10%: 95
cases where length improved by at least 30%: 57
cases where length worsened by at least 5%: 36
cases where length worsened by at least 10%: 26
cases where length worsened by at least 30%: 14
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 99
cases where the resets improved by at least 10%: 99
cases where the resets improved by at least 30%: 92
cases where the resets worsened by at least 5%: 37
cases where the resets worsened by at least 10%: 24
cases where the resets worsened by at least 30%: 13
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


50 or more inputs

cases that passed the filter: 83
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 48
cases where length improved by at least 10%: 48
cases where length improved by at least 30%: 32
cases where length worsened by at least 5%: 4
cases where length worsened by at least 10%: 4
cases where length worsened by at least 30%: 4
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 48
cases where the resets improved by at least 10%: 48
cases where the resets improved by at least 30%: 48
cases where the resets worsened by at least 5%: 4
cases where the resets worsened by at least 10%: 4
cases where the resets worsened by at least 30%: 4
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


70 or more states and 30 or more inputs

cases that passed the filter: 47
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 30
cases where length improved by at least 10%: 30
cases where length improved by at least 30%: 15
cases where length worsened by at least 5%: 6
cases where length worsened by at least 10%: 6
cases where length worsened by at least 30%: 6
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 30
cases where the resets improved by at least 10%: 30
cases where the resets improved by at least 30%: 30
cases where the resets worsened by at least 5%: 6
cases where the resets worsened by at least 10%: 6
cases where the resets worsened by at least 30%: 6
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


just the cases where the ads method is not applicable

all results

cases that passed the filter: 186
cases where length and reset changes did not match up: 8
cases where length improved by at least 5%: 131
cases where length improved by at least 10%: 121
cases where length improved by at least 30%: 65
cases where length worsened by at least 5%: 45
cases where length worsened by at least 10%: 42
cases where length worsened by at least 30%: 22
cases where length worsened by at least 50%: 12
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 138
cases where the resets improved by at least 10%: 135
cases where the resets improved by at least 30%: 105
cases where the resets worsened by at least 5%: 36
cases where the resets worsened by at least 10%: 31
cases where the resets worsened by at least 30%: 12
cases where the resets worsened by at least 50%: 7
cases where the resets worsened by at least 100%: 4


50 or more states

cases that passed the filter: 51
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 46
cases where length improved by at least 10%: 46
cases where length improved by at least 30%: 16
cases where length worsened by at least 5%: 5
cases where length worsened by at least 10%: 5
cases where length worsened by at least 30%: 5
cases where length worsened by at least 50%: 5
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 46
cases where the resets improved by at least 10%: 46
cases where the resets improved by at least 30%: 45
cases where the resets worsened by at least 5%: 5
cases where the resets worsened by at least 10%: 5
cases where the resets worsened by at least 30%: 5
cases where the resets worsened by at least 50%: 5
cases where the resets worsened by at least 100%: 4


200 or more states

cases that passed the filter: 33
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 29
cases where length improved by at least 10%: 29
cases where length improved by at least 30%: 7
cases where length worsened by at least 5%: 4
cases where length worsened by at least 10%: 4
cases where length worsened by at least 30%: 4
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 29
cases where the resets improved by at least 10%: 29
cases where the resets improved by at least 30%: 29
cases where the resets worsened by at least 5%: 4
cases where the resets worsened by at least 10%: 4
cases where the resets worsened by at least 30%: 4
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


10 or more inputs

cases that passed the filter: 109
cases where length and reset changes did not match up: 1
cases where length improved by at least 5%: 99
cases where length improved by at least 10%: 95
cases where length improved by at least 30%: 57
cases where length worsened by at least 5%: 10
cases where length worsened by at least 10%: 10
cases where length worsened by at least 30%: 5
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 99
cases where the resets improved by at least 10%: 99
cases where the resets improved by at least 30%: 92
cases where the resets worsened by at least 5%: 8
cases where the resets worsened by at least 10%: 8
cases where the resets worsened by at least 30%: 4
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


50 or more inputs

cases that passed the filter: 52
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 48
cases where length improved by at least 10%: 48
cases where length improved by at least 30%: 32
cases where length worsened by at least 5%: 4
cases where length worsened by at least 10%: 4
cases where length worsened by at least 30%: 4
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 48
cases where the resets improved by at least 10%: 48
cases where the resets improved by at least 30%: 48
cases where the resets worsened by at least 5%: 4
cases where the resets worsened by at least 10%: 4
cases where the resets worsened by at least 30%: 4
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


70 or more states and 30 or more inputs

cases that passed the filter: 34
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 30
cases where length improved by at least 10%: 30
cases where length improved by at least 30%: 15
cases where length worsened by at least 5%: 4
cases where length worsened by at least 10%: 4
cases where length worsened by at least 30%: 4
cases where length worsened by at least 50%: 4
cases where length worsened by at least 100%: 4
cases where the resets improved by at least 5%: 30
cases where the resets improved by at least 10%: 30
cases where the resets improved by at least 30%: 30
cases where the resets worsened by at least 5%: 4
cases where the resets worsened by at least 10%: 4
cases where the resets worsened by at least 30%: 4
cases where the resets worsened by at least 50%: 4
cases where the resets worsened by at least 100%: 4


just the cases where the ads method gives almost no info

all results

cases that passed the filter: 177
cases where length and reset changes did not match up: 8
cases where length improved by at least 5%: 126
cases where length improved by at least 10%: 120
cases where length improved by at least 30%: 65
cases where length worsened by at least 5%: 41
cases where length worsened by at least 10%: 38
cases where length worsened by at least 30%: 18
cases where length worsened by at least 50%: 8
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 133
cases where the resets improved by at least 10%: 130
cases where the resets improved by at least 30%: 105
cases where the resets worsened by at least 5%: 32
cases where the resets worsened by at least 10%: 27
cases where the resets worsened by at least 30%: 8
cases where the resets worsened by at least 50%: 3
cases where the resets worsened by at least 100%: 0


50 or more states

cases that passed the filter: 47
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 46
cases where length improved by at least 10%: 46
cases where length improved by at least 30%: 16
cases where length worsened by at least 5%: 1
cases where length worsened by at least 10%: 1
cases where length worsened by at least 30%: 1
cases where length worsened by at least 50%: 1
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 46
cases where the resets improved by at least 10%: 46
cases where the resets improved by at least 30%: 45
cases where the resets worsened by at least 5%: 1
cases where the resets worsened by at least 10%: 1
cases where the resets worsened by at least 30%: 1
cases where the resets worsened by at least 50%: 1
cases where the resets worsened by at least 100%: 0


200 or more states

cases that passed the filter: 29
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 29
cases where length improved by at least 10%: 29
cases where length improved by at least 30%: 7
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 29
cases where the resets improved by at least 10%: 29
cases where the resets improved by at least 30%: 29
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


10 or more inputs

cases that passed the filter: 101
cases where length and reset changes did not match up: 1
cases where length improved by at least 5%: 95
cases where length improved by at least 10%: 95
cases where length improved by at least 30%: 57
cases where length worsened by at least 5%: 6
cases where length worsened by at least 10%: 6
cases where length worsened by at least 30%: 1
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 95
cases where the resets improved by at least 10%: 95
cases where the resets improved by at least 30%: 92
cases where the resets worsened by at least 5%: 4
cases where the resets worsened by at least 10%: 4
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


50 or more inputs

cases that passed the filter: 48
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 48
cases where length improved by at least 10%: 48
cases where length improved by at least 30%: 32
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 48
cases where the resets improved by at least 10%: 48
cases where the resets improved by at least 30%: 48
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


70 or more states and 30 or more inputs

cases that passed the filter: 30
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 30
cases where length improved by at least 10%: 30
cases where length improved by at least 30%: 15
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 30
cases where the resets improved by at least 10%: 30
cases where the resets improved by at least 30%: 30
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


just the cases where the ads method gives no info

all results

cases that passed the filter: 124
cases where length and reset changes did not match up: 7
cases where length improved by at least 5%: 90
cases where length improved by at least 10%: 84
cases where length improved by at least 30%: 57
cases where length worsened by at least 5%: 24
cases where length worsened by at least 10%: 22
cases where length worsened by at least 30%: 9
cases where length worsened by at least 50%: 5
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 97
cases where the resets improved by at least 10%: 94
cases where the resets improved by at least 30%: 70
cases where the resets worsened by at least 5%: 19
cases where the resets worsened by at least 10%: 14
cases where the resets worsened by at least 30%: 5
cases where the resets worsened by at least 50%: 2
cases where the resets worsened by at least 100%: 0


50 or more states

cases that passed the filter: 16
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 15
cases where length improved by at least 10%: 15
cases where length improved by at least 30%: 13
cases where length worsened by at least 5%: 1
cases where length worsened by at least 10%: 1
cases where length worsened by at least 30%: 1
cases where length worsened by at least 50%: 1
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 15
cases where the resets improved by at least 10%: 15
cases where the resets improved by at least 30%: 15
cases where the resets worsened by at least 5%: 1
cases where the resets worsened by at least 10%: 1
cases where the resets worsened by at least 30%: 1
cases where the resets worsened by at least 50%: 1
cases where the resets worsened by at least 100%: 0


200 or more states

cases that passed the filter: 5
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 5
cases where length improved by at least 10%: 5
cases where length improved by at least 30%: 4
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 5
cases where the resets improved by at least 10%: 5
cases where the resets improved by at least 30%: 5
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


10 or more inputs

cases that passed the filter: 61
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 60
cases where length improved by at least 10%: 60
cases where length improved by at least 30%: 50
cases where length worsened by at least 5%: 1
cases where length worsened by at least 10%: 1
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 60
cases where the resets improved by at least 10%: 60
cases where the resets improved by at least 30%: 58
cases where the resets worsened by at least 5%: 1
cases where the resets worsened by at least 10%: 1
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


50 or more inputs

cases that passed the filter: 26
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 26
cases where length improved by at least 10%: 26
cases where length improved by at least 30%: 25
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 26
cases where the resets improved by at least 10%: 26
cases where the resets improved by at least 30%: 26
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0


70 or more states and 30 or more inputs

cases that passed the filter: 12
cases where length and reset changes did not match up: 0
cases where length improved by at least 5%: 12
cases where length improved by at least 10%: 12
cases where length improved by at least 30%: 12
cases where length worsened by at least 5%: 0
cases where length worsened by at least 10%: 0
cases where length worsened by at least 30%: 0
cases where length worsened by at least 50%: 0
cases where length worsened by at least 100%: 0
cases where the resets improved by at least 5%: 12
cases where the resets improved by at least 10%: 12
cases where the resets improved by at least 30%: 12
cases where the resets worsened by at least 5%: 0
cases where the resets worsened by at least 10%: 0
cases where the resets worsened by at least 30%: 0
cases where the resets worsened by at least 50%: 0
cases where the resets worsened by at least 100%: 0



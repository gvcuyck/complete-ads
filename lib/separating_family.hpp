//
// Created by gijs on 27-10-2018.
//


#ifndef COMPLETE_ADS_SEPARATING_FAMILY_H
#define COMPLETE_ADS_SEPARATING_FAMILY_H

#include "types.hpp"
#include "splitting_tree.hpp"
#include <vector>
namespace complete
{
//a set of separators that you can use to differentiate one specific state from all the other states.
    using separating_set = std::vector<word>;
//a set of separating sets that you can use to differentiate every state from every other state.
    using separating_family = std::vector<separating_set>;

//creates a separating family by creating a single separating set for every state in the specification.
//the main focus of this separating family is checking if you are in a specific state while you already have some idea of where you are.
//you can then use one specific separating set to confirm or disprove that idea, without needing the whole separating family.
    separating_family create_separating_family(const splitting_tree &tree, const mealy &specification);

//outputs a separating family to an ostream.
    std::ostream &separating_family_to_stream(std::ostream &s, const separating_family &family,
                                              const std::vector<std::string> &inputs,
                                              const std::vector<std::string> &states);

//outputs a separating set to an ostream.
    std::ostream &separating_set_to_stream(std::ostream &s, const separating_set &set, size_t indent_amount,
                                           const std::vector<std::string> &inputs);

//returns true if family truly behaves as a separating family.
//so for every separating set in the family,
//if you apply that set to its target and then apply it to a random state that is not its target, the outputs have to be different.
    bool test_separating_family(const separating_family &family, const mealy &specification);
}
#endif //COMPLETE_ADS_SEPARATING_FAMILY_H

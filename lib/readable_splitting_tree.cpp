//
// Created by Gijs van Cuyck on 19/10/2018.
//
#include "readable_splitting_tree.hpp"
#include "vector_printing.hpp"
#include "read_mealy.hpp"
#include <algorithm>


namespace complete
{

    readable_splitting_tree translate_splitting_tree(const splitting_tree &tree, const std::vector<std::string> &inputs,
                                                     const std::vector<std::string> &states)
    {
        readable_splitting_tree translated_tree(tree.states.size(), tree.depth);
        transform(tree.states.begin(), tree.states.end(), translated_tree.states.begin(), [&states](state s)
        { return states[s]; });
        for (int i = 0; i < tree.separators.size(); i++)
        {
            std::vector<std::string> new_seperator(tree.separators[i].size());
            transform(tree.separators[i].begin(), tree.separators[i].end(), new_seperator.begin(), [&inputs](input i)
            { return inputs[i]; });
            translated_tree.separators.push_back(new_seperator);
        }
        for (std::pair<state, size_t> kv: tree.valid_map)
        {
            translated_tree.valid_map[states[kv.first]] = kv.second;
        }
        for (std::vector<splitting_tree> children : tree.children)
        {
            std::vector<readable_splitting_tree> new_children(children.size());
            transform(children.begin(), children.end(), new_children.begin(),
                      [&inputs, &states](splitting_tree &child)
                      {
                          return translate_splitting_tree(child, inputs, states);
                      });
            translated_tree.children.push_back(new_children);
        }
        return translated_tree;
    }

    std::ostream &rst_to_stream(readable_splitting_tree &tree, std::ostream &output, int ident)
    {
        std::string indentations(ident, '\t');
        std::vector<std::vector<std::string>> reverse_valid_map;
        if (tree.children.size() > 1)
            reverse_valid_map = create_reverse_map(tree.valid_map, tree.children.size());


        output << indentations << "{" << join(tree.states.begin(), tree.states.end(), ",", "}") << "\n";
        for (int i = 0; i < tree.children.size(); i++)
        {
            output << indentations << "seperator target: ";
            if (reverse_valid_map.empty())
                output << "all states\n";
            else
                output << "{" << join(reverse_valid_map[i].begin(), reverse_valid_map[i].end(), ",", "}") << "\n";
            output << indentations << join(tree.separators[i].begin(), tree.separators[i].end(), ",") << "\n";
            for (auto &child : tree.children[i])
            {
                rst_to_stream(child, output, ident + 1);
            }
        }
        return output;


    }

    std::ostream &rst_to_stream(readable_splitting_tree &tree, std::ostream &output)
    { return rst_to_stream(tree, output, 0); }

}
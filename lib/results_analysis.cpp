//
// Created by gijsc on 2-1-2019.
//
#include "results_analysis.hpp"
#include <fstream>
#include <vector>
#include <iostream>
#include <regex>
#include <cassert>

namespace analysis
{
    const std::regex integer("(-?[0-9]+)");
    //either an integer or an optional integer followed by . followed by a positive integer.
    const std::regex real_number("(-?[0-9]*\\.[0-9]+)|(-?[0-9]+)");

    //return the first part of the first line of the inputstream that matches the regex.
    std::string read_regex(const std::regex &regex, std::ifstream &is)
    {
        std::smatch m;
        std::string line;
        std::getline(is, line);
        bool found_the_regex = std::regex_search(line, m, regex);
        assert(found_the_regex);
        return m[0];
    }

    //returns the first integer on the first line of the input stream
    int read_int(std::ifstream &is)
    {
        return stoi(read_regex(integer, is));
    }

    //returns the first double on the first line of the inputstream.
    double read_double(std::ifstream &is)
    {
        return stod(read_regex(real_number, is));
    }

    struct result
    {
        //amount of states in the input graph
        int states;
        //amount of different inputs in the input graph
        int inputs;
        //amount of states the ads method reduce a state identification query to on average.
        int average_uncertainty;
        //amount of states the ads method reduce a state identification query to in the worst case.
        int max_uncertainty;
        //percentage difference between the amount of states and the average uncertainty.
        double average_uncertainty_gain;
        //percentage difference between the amount of states and the max uncertainty.
        double max_uncertainty_gain;
        //the 'length' of the test suite as generated by the complete ADS method
        int complete_ts_length;
        //the amount of resets in test suite as generated by the complete ADS method
        int complete_ts_resets;
        //the 'length' of the test suite as generated by the hybrid ADS method
        int hybrid_ts_length;
        //the amount of resets in test suite as generated by the hybrid ADS method
        int hybrid_ts_resets;
        //percentage difference between the complete and hybrid ts length
        double length_gain;
        //percentage difference between the complete and hybrid ts reset amount.
        double resets_gain;

    };

    result read_results_from_stream(std::ifstream &is)
    {
        is.ignore(5000, '\n');
        is.ignore(5000, '\n');
        int states = read_int(is);
        int inputs = read_int(is);
        is.ignore(5000, '\n');
        int average_uncertainty = read_int(is);
        int max_uncertainty = read_int(is);
        double average_uncertainty_gain = read_double(is);
        double max_uncertainty_gain = read_double(is);
        is.ignore(5000, '\n');
        int complete_ts_length = read_int(is);
        int complete_ts_resets = read_int(is);
        is.ignore(5000, '\n');
        int hybrid_ts_length = read_int(is);
        int hybrid_ts_resets = read_int(is);
        is.ignore(5000, '\n');
        double length_gain = read_double(is);
        double resets_gain = read_double(is);
        return result{states, inputs, average_uncertainty, max_uncertainty, average_uncertainty_gain,
                      max_uncertainty_gain,
                      complete_ts_length, complete_ts_resets, hybrid_ts_length, hybrid_ts_resets, length_gain,
                      resets_gain};
    }

    void parse_file(const std::string &input_filename, std::vector<result> &results)
    {
        std::ifstream input_file(input_filename);
        while (!input_file.eof())
        {
            char lookahead = input_file.peek();
            if (lookahead == '\n' || input_file.eof())
            {
                input_file.ignore(1, '\n');
            } else if (lookahead == '#')
            {
                input_file.ignore(5000, '\n');
            } else
            {
                results.emplace_back(read_results_from_stream(input_file));
            }
        }
    }

    void analyze_with_filter( std::ostream &output, std::function<bool(result)> filter,const std::vector<result> & results)
    {
        int total_cases =0;
        int ambigu = 0;
        int len_impr_5 = 0;
        int len_impr_10 = 0;
        int len_impr_30 = 0;
        int len_decr_5 = 0;
        int len_decr_10 = 0;
        int len_decr_30 = 0;
        int len_decr_50 = 0;
        int len_decr_100 = 0;

        int res_impr_5 = 0;
        int res_impr_10 = 0;
        int res_impr_30 = 0;
        int res_decr_5 = 0;
        int res_decr_10 = 0;
        int res_decr_30 = 0;
        int res_decr_50 = 0;
        int res_decr_100 = 0;

        for (const result & res:results)
        {
            if(!filter(res)||res.states==1)
                continue;
            total_cases+=1;
            if((res.length_gain>0 && res.resets_gain <0)||(res.length_gain<0 && res.resets_gain>0))
                ambigu+=1;
            if(res.length_gain>=30)
                len_impr_30+=1;
            if(res.length_gain>=10)
                len_impr_10+=1;
            if(res.length_gain>=5)
                len_impr_5+=1;
            if(res.length_gain<=-5)
                len_decr_5+=1;
            if(res.length_gain<=-10)
                len_decr_10+=1;
            if(res.length_gain<=-30)
                len_decr_30+=1;
            if(res.length_gain<=-50)
                len_decr_50+=1;
            if(res.length_gain<=-100)
                len_decr_100+=1;

            if(res.resets_gain>=30)
                res_impr_30+=1;
            if(res.resets_gain>=10)
                res_impr_10+=1;
            if(res.resets_gain>=5)
                res_impr_5+=1;
            if(res.resets_gain<=-5)
                res_decr_5+=1;
            if(res.resets_gain<=-10)
                res_decr_10+=1;
            if(res.resets_gain<=-30)
                res_decr_30+=1;
            if(res.resets_gain<=-50)
                res_decr_50+=1;
            if(res.resets_gain<=-100)
                res_decr_100+=1;
        }
        output << "cases that passed the filter: " << total_cases << "\n";
        output << "cases where length and reset changes did not match up: " << ambigu <<"\n";
        output << "cases where length improved by at least 5%: " << len_impr_5 <<"\n";
        output << "cases where length improved by at least 10%: " << len_impr_10 <<"\n";
        output << "cases where length improved by at least 30%: " << len_impr_30 <<"\n";
        output << "cases where length worsened by at least 5%: " << len_decr_5<<"\n";
        output << "cases where length worsened by at least 10%: " << len_decr_10 <<"\n";
        output << "cases where length worsened by at least 30%: " << len_decr_30 <<"\n";
        output << "cases where length worsened by at least 50%: " << len_decr_50 <<"\n";
        output << "cases where length worsened by at least 100%: " << len_decr_100 <<"\n";
        output << "cases where the resets improved by at least 5%: " << res_impr_5 <<"\n";
        output << "cases where the resets improved by at least 10%: " << res_impr_10 <<"\n";
        output << "cases where the resets improved by at least 30%: " << res_impr_30 <<"\n";
        output << "cases where the resets worsened by at least 5%: " << res_decr_5 <<"\n";
        output << "cases where the resets worsened by at least 10%: " << res_decr_10 <<"\n";
        output << "cases where the resets worsened by at least 30%: " << res_decr_30 <<"\n";
        output << "cases where the resets worsened by at least 50%: " << res_decr_50 <<"\n";
        output << "cases where the resets worsened by at least 100%: " << res_decr_100 <<"\n";

    }


    void analyze_results(std::string input_filename, std::string output_directory)
    {
        size_t start = input_filename.find_last_of('/');
        if (start == std::string::npos)
            start = input_filename.find_last_of('\\');
        size_t end = input_filename.find_last_of('.');
        std::string output_filename = output_directory+input_filename.substr(start, end - start) + "_analysis_results.txt";
        std::vector<result> results;
        parse_file(input_filename, results);
        std::ofstream output_file(output_filename);
        output_file << "no extra filter\n\n";
        output_file << "all results"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return true;},results);
        output_file << "\n\n";
        output_file << "50 or more states"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return res.states>=50;},results);
        output_file << "\n\n";
        output_file << "200 or more states"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return res.states>=200;},results);
        output_file << "\n\n";
        output_file << "10 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return res.inputs>=10;},results);
        output_file << "\n\n";
        output_file << "50 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return res.inputs>=50;},results);
        output_file << "\n\n";
        output_file << "70 or more states and 30 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[](const result & res){ return res.states >=70 && res.inputs>=30;},results);
        output_file << "\n\n";

        output_file<< "just the cases where the ads method is not applicable\n\n";
        std::function<bool(result)> ads_applicable = [](const result& res){ return res.max_uncertainty==1;};
        output_file << "all results"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return !ads_applicable(res);},results);
        output_file << "\n\n";
        output_file << "50 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return res.states>=50 && !ads_applicable(res);},results);
        output_file << "\n\n";
        output_file << "200 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return res.states>=200&& !ads_applicable(res);},results);
        output_file << "\n\n";
        output_file << "10 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return res.inputs>=10&& !ads_applicable(res);},results);
        output_file << "\n\n";
        output_file << "50 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return res.inputs>=50&& !ads_applicable(res);},results);
        output_file << "\n\n";
        output_file << "70 or more states and 30 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_applicable](const result & res){ return res.states >=70 && res.inputs>=30&& !ads_applicable(res);},results);
        output_file << "\n\n";

        output_file<< "just the cases where the ads method gives almost no info\n\n";
        std::function<bool(result)> ads_somewhat_applicable = [](const result& res){ return res.max_uncertainty<=res.states/2;};
        output_file << "all results"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";
        output_file << "50 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return res.states>=50 && !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";
        output_file << "200 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return res.states>=200&& !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";
        output_file << "10 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return res.inputs>=10&& !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";
        output_file << "50 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return res.inputs>=50&& !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";
        output_file << "70 or more states and 30 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_somewhat_applicable](const result & res){ return res.states >=70 && res.inputs>=30&& !ads_somewhat_applicable(res);},results);
        output_file << "\n\n";

        output_file<< "just the cases where the ads method gives no info\n\n";
        std::function<bool(result)> ads_no_extra_info = [](const result& res){ return res.max_uncertainty==res.states;};
        output_file << "all results"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return ads_no_extra_info(res);},results);
        output_file << "\n\n";
        output_file << "50 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return res.states>=50 && ads_no_extra_info(res);},results);
        output_file << "\n\n";
        output_file << "200 or more states"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return res.states>=200&& ads_no_extra_info(res);},results);
        output_file << "\n\n";
        output_file << "10 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return res.inputs>=10&& ads_no_extra_info(res);},results);
        output_file << "\n\n";
        output_file << "50 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return res.inputs>=50&& ads_no_extra_info(res);},results);
        output_file << "\n\n";
        output_file << "70 or more states and 30 or more inputs"<<"\n\n";
        analyze_with_filter(output_file,[&ads_no_extra_info](const result & res){ return res.states >=70 && res.inputs>=30&& ads_no_extra_info(res);},results);
        output_file << "\n\n";



    }
}

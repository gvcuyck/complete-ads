#pragma once

#include "mealy.hpp"
#include <unordered_map>
#include <stdexcept>

/// \brief A splitting tree as defined in Lee & Yannakakis.
/// This is also known as a derivation tree (Knuutila). Both the Gill/Moore/Hopcroft-style and the
/// Lee&Yannakakis-style trees are splitting trees.
namespace complete
{
	struct splitting_tree
	{
		splitting_tree(size_t N, size_t depth);

		std::vector<state> states;
		std::vector<std::vector<splitting_tree>> children;
		std::vector<word> separators;
		size_t depth = 0;
		std::unordered_map<state, size_t> valid_map;

		const std::vector<splitting_tree> &get_children(state s) const
		{
			if (children.empty())
				throw std::runtime_error("get_children called when there were no children");
			else if (children.size() == 1)
				return children.front();
			else if (valid_map.empty() || valid_map.count(s) < 1)
				throw std::runtime_error("valid map does not contain required information");
			else
				return children[valid_map.at(s)];
		}

		//returns the first set of children if the target is not found in valid list.
		//otherwise behaves exactly the same as get_children.
		//this behaviour is required for the lca_impl, but might mask errors if used elsewere.
		const std::vector<splitting_tree> &get_children_reliably(state s) const
		{
			if (children.empty())
				throw std::runtime_error("get_children called when there were no children");
			else if (valid_map.count(s) < 1)
				return children.front();
			else
				return children[valid_map.at(s)];
		}

		const word &get_separator(state s) const
		{
			if (children.empty())
				throw std::runtime_error("get_separator called when there were no separators");
			else if (children.size() == 1)
				return separators.front();
			else if (valid_map.empty() || valid_map.count(s) < 1)
				throw std::runtime_error("valid map does not contain required information");
			else
				return separators[valid_map.at(s)];
		}
	};

/// \brief the generic lca implementation.
/// It uses \p store to store the relevant nodes (in some bottom up order), the last store is the
/// actual lowest common ancestor (but the other might be relevant as well). The function \p f is
/// the predicate on the states (returns true for the states we want to compute the lca of).
/// valid_target is used to select a set of children when a node contains multiple separators.
/// The target can only be one node at a time for complexity reasons.
	template<typename Fun, typename Store>
	size_t lca_impl(splitting_tree const &node, Fun &&f, Store &&store, state valid_target)
	{
		static_assert(std::is_same<decltype(f(state(0))), bool>::value, "f should return a bool");
		if (node.children.empty())
		{
			// if it is a leaf, we search for the states
			// if it contains a state, return this leaf
			for (auto s : node.states)
			{
				if (f(s))
				{
					store(node);
					return 1;
				}
			}
			// did not contain the leaf => nullptr
			return 0;
		} else
		{
			// otherwise, check our children. If there is a single one giving a node
			// we return this (it's the lca), if more children return a non-nil
			// node, then we are the lca
			size_t count = 0;
			for (const splitting_tree &c : node.get_children_reliably(valid_target))
			{
				auto inner_count = lca_impl(c, f, store, valid_target);
				if (inner_count > 0) count++;
			}

			if (count >= 2)
			{
				store(node);
			}

			return count;
		}
	}

/// \brief Find the lowest common ancestor of elements on which \p f returns true.
	template<typename Fun>
	splitting_tree &lca(splitting_tree &root, Fun &&f, state valid_target)
	{
		splitting_tree const *store = nullptr;
		lca_impl(root, f, [&store](splitting_tree const &node)
		{ store = &node; }, valid_target);
		return const_cast<splitting_tree &>(*store); // NOTE: this const_cast is safe
	}

	template<typename Fun>
	const splitting_tree &lca(const splitting_tree &root, Fun &&f, state valid_target)
	{
		splitting_tree const *store = nullptr;
		lca_impl(root, f, [&store](splitting_tree const &node)
		{ store = &node; }, valid_target);
		return *store;
	}

/// \brief Creates a splitting tree by partition refinement.
	splitting_tree create_splitting_tree(mealy const &m);
}
//
// Created by gijs on 27-10-2018.
//

#include "separating_family.hpp"
#include "vector_printing.hpp"
#include "types.hpp"
#include <list>
#include <algorithm>
#include <cassert>

namespace complete
{

    std::ostream &separating_set_to_stream(std::ostream &s, const separating_set &set, const size_t indent_amount,
                                           const std::vector<std::string> &inputs)
    {
        const std::string indents(indent_amount, '\t');
        for (word w : set)
        {
            std::vector<std::string> translated_word(w.size());
            transform(w.begin(), w.end(), translated_word.begin(), [&inputs](size_t symbol_index)
            { return inputs[symbol_index]; });
            s << indents << join(translated_word.begin(), translated_word.end(), ",", "\n");
        }
        return s;

    }

    std::ostream &
    separating_family_to_stream(std::ostream &s, const separating_family &family,
                                const std::vector<std::string> &inputs,
                                const std::vector<std::string> &states)
    {
        for (state i = 0; i < family.size(); i++)
        {
            s << "separators for state: " << states[i] << "\n";
            separating_set_to_stream(s, family[i], 1, inputs) << "\n";
        }
        return s;

    }


    void update_CI_mapping(std::unordered_map<state, std::vector<state>> &CI_mapping, const output target_output,
                           const mealy &specification, const word &separator)
    {
        auto it = CI_mapping.begin();
        const auto start = separator.begin();
        const auto end = separator.end();

        //updating the CI list involves changing the keys of values, and merging certain values together under a single key.
        //this is hard to do in-place, so we use this buffer map to hold the output and then switch it with the CI_mapping at the end.
        std::unordered_map<state, std::vector<state>> ret;
        //reserve for the maximum posible size to prevent rehashes.
        ret.reserve(CI_mapping.size() - 1);

        while (it != CI_mapping.end())
        {
            mealy::edge result = apply(specification, it->first, start, end);
            if (result.out != target_output)
            {
                it = CI_mapping.erase(it);
            } else
            {
                std::vector<state> &possible_existing_value = ret[result.to];
                if (possible_existing_value.empty())
                    ret[result.to] = it->second;
                else if (possible_existing_value.size() > it->second.size())
                    possible_existing_value.insert(possible_existing_value.end(), it->second.begin(), it->second.end());
                else
                {
                    it->second.insert(it->second.end(), possible_existing_value.begin(), possible_existing_value.end());
                    ret[result.to] = move(it->second);
                }
                it++;

            }
        }
        CI_mapping = move(ret);

    }

    separating_family create_separating_family(const splitting_tree &tree, const mealy &specification)
    {
        separating_family ret;
        for (state global_target = 0; global_target < specification.graph_size; global_target++)
        {
            state target = global_target;
            separating_set sep_set(1);
            std::unordered_map<state, std::vector<state>> CI_mapping;
            CI_mapping.reserve(specification.graph_size);
            for (state s = 0; s < specification.graph_size; s++)
            {
                CI_mapping[s] = {s};
            }
            while (CI_mapping.size() > 1)
            {
                std::vector<bool> states(tree.states.size(), false);
                for (std::pair<state, std::vector<state>> &&C_I_pair : CI_mapping)
                {
                    states[C_I_pair.first] = true;
                }

                const splitting_tree &oboom = lca(tree, [&states](state state) -> bool
                {
                    return states[state];
                }, target);

                const word &separator = oboom.get_separator(target);
                sep_set.back().insert(sep_set.back().end(), separator.begin(), separator.end());
                mealy::edge target_result = apply(specification, target, separator.begin(), separator.end());
                target = target_result.to;
                update_CI_mapping(CI_mapping, target_result.out, specification, separator);

                if (CI_mapping.size() == 1 && CI_mapping.begin()->second.size() > 1)
                {
                    //if we get here then the size of the current set is 1 but the size of the initial set is still bigger than 1.
                    //to solve this we perform a reset operation, and bring the current set back to be the initial set.
                    const std::vector<state> initial_set = move(CI_mapping.begin()->second);
                    CI_mapping.clear();
                    CI_mapping.reserve(initial_set.size());
                    for (state s : initial_set)
                    {
                        CI_mapping[s] = {s};
                    }
                    sep_set.emplace_back();
                    target = global_target;

                }
            }
            assert(CI_mapping.size() == 1);
            assert(CI_mapping.begin()->second.size() == 1);
            assert(CI_mapping.begin()->second[0] == global_target);
            ret.push_back(move(sep_set));

        }
        return ret;
    }


    bool test_separating_family(const separating_family &family, const mealy &specification)
    {
        assert(family.size() == specification.graph_size);
        for (state s = 0; s < family.size(); s++)
        {
            state target = s;
            std::list<std::pair<state, state>> CI_list(specification.graph_size);
            auto CI_it = CI_list.begin();
            for (state i = 0; i < family.size(); i++)
            {
                (*CI_it).first = i;
                (*CI_it).second = i;
                CI_it++;
            }
            for (const word &word : family[s])
            {
                for (const input symbol : word)
                {
                    mealy::edge expected = apply(specification, target, symbol);
                    target = expected.to;
                    auto it = CI_list.begin();
                    while (it != CI_list.end())
                    {
                        mealy::edge observed = apply(specification, (*it).first, symbol);
                        if (observed.out != expected.out)
                        {
                            it = CI_list.erase(it);
                        } else
                        {
                            (*it).first = observed.to;
                            it++;
                        }
                    }
                }
                if (CI_list.size() > 1)
                {
                    for (auto &CI_pair : CI_list)
                    {
                        CI_pair.first = CI_pair.second;
                    }
                    target = s;
                }
            }
            if (CI_list.size() > 1)
                return false;
        }
        return true;

    }
}


#include "splitting_tree.hpp"
#include "partition.hpp"
#include "types.hpp"
#include "vector_printing.hpp"

#include <algorithm>
#include <cassert>
#include <functional>
#include <queue>
#include <random>
#include <unordered_set>
#include <tuple>
#include <iostream>

namespace complete
{
    using namespace std;

    splitting_tree::splitting_tree(size_t N, size_t d) : states(N), depth(d)
    {
        iota(begin(states), end(states), 0);
    }


/// \brief struct used to combine splitting trees with additional data that wont end up in the result but is needed
/// for some computations.
    struct work_set
    {

        work_set(splitting_tree &tree, unordered_set<state> &&valid, bool do_output_split) : work(tree), valid_states(
                forward<unordered_set<state>>(valid))
        {
            split_on_output = do_output_split;
        }

        work_set(splitting_tree &tree, bool do_output_split) : work(tree)
        {
            split_on_output = do_output_split;
        }

        splitting_tree &work;
        /// \brief the set of states for wich the current tree tries to be valid.
        /// to conserve space, the empty set is used to indicate the tree tries to be valid for all states.
        unordered_set<state> valid_states;

        /// \brief whether to check for splits based on output or on state.
        bool split_on_output;
    };


//removes every element in update from original. returns true if any elements were removed.
    bool update_valid_set(unordered_set<state> &original, const unordered_set<state> &update)
    {
        bool ret = false;
        for (state s : update)
        {
            if (original.erase(s) > 0)
                ret = true;
        }
        return ret;

    }

//returns the set of states for which the input symbol is valid,
//assuming the input is a partition of those states based on the output they give when symbol is applied.
    unordered_set<state> is_valid_for(const size_t &N, const mealy &g, list<list<state>> const &blocks, input symbol)
    {

        unordered_set<state> ret;
        for (auto &&block : blocks)
        {

            const auto new_blocks = partition_(begin(block), end(block), [symbol, &g](state state)
            {
                return apply(g, state, symbol).to;
            }, N);
            for (auto &&new_block : new_blocks)
            {
                if (new_block.size() == 1)
                    ret.emplace(new_block.front());
            }

        }
        return ret;
    };

    void add_push_new_block(deque<work_set> &work_list, list<list<state>> const &new_blocks, splitting_tree &boom,
                            unordered_set<state> &valid_list, bool update_valid_map)
    {
        boom.children.emplace_back();
        boom.children.back().assign(new_blocks.size(), splitting_tree(0, boom.depth + 1));

        size_t i = 0;
        for (auto &&b : new_blocks)
        {
            boom.children.back()[i++].states.assign(begin(b), end(b));
        }
        for (auto &&c : boom.children.back())
        {
            //reduce valid list to those states that are actually relevant.
            unordered_set<state> new_valid_list;
            if (!valid_list.empty())
            {
                for (state s : c.states)
                {
                    if (valid_list.count(s) > 0)
                        new_valid_list.emplace(s);
                }
                if (new_valid_list.empty())
                {
                    //this means that none of the states of this child are actually valid for this split.
                    //this child will therefore never be used by the algorithm and can be pruned.
                    continue;
                }
                if (new_valid_list.size() == c.states.size())
                {
                    //this means that the new child is valid for all its states, and that we can leave the valid list empty to signal that and save space.
                    new_valid_list.clear();
                }
            }
            work_list.push_front(work_set(c, move(new_valid_list), true));
        }

        //if required, update the valid_map of the tree to indicate which of the children and separators are valid for which state.
        if (update_valid_map)
        {
            size_t index = boom.children.size() - 1;
            for (state s : valid_list)
            {
                if (boom.valid_map.count(s) > 0)
                {
                    //when multiple separators are valid, we prefer the shorter one.
                    if (boom.separators[boom.valid_map[s]].size() > boom.separators.back().size())
                    {
                        //the new one was shorter
                        boom.valid_map[s] = index;
                    }
                } else
                {
                    boom.valid_map[s] = index;
                }
            }
        }

        assert(boom.children.size() == boom.separators.size());
        if (boom.children.size() > 1)
            assert(!boom.valid_map.empty());
        assert(boom.states.size() == accumulate(begin(boom.children.back()), end(boom.children.back()), 0ul,
                                                [](size_t l, const splitting_tree &r)
                                                {
                                                    return l + r.states.size();
                                                }));

    };


    splitting_tree create_splitting_tree(const mealy &g)
    {
        const auto N = g.graph_size;
        const auto P = g.input_size;
        const auto Q = g.output_size;

        splitting_tree root(N, 0);


        // We'll use a deque to keep track of leaves we have to investigate;
        // In some cases we cannot split, and have to wait for other parts of the
        // tree. We keep track of how many times we did no work. If this is too
        // much, there is no complete splitting tree.
        deque<work_set> work_list;
        size_t days_without_progress = 0;

        //when set to true the algorithm will select a split as best as it can for the current work set, even when there are no "valid" options.
        bool force_progress = false;

        vector<input> all_inputs(P);
        iota(begin(all_inputs), end(all_inputs), 0);


        // Some lambda functions capturing some state, makes the code a bit easier :)


        //returns true if the given valid_set of states is valid for input symbol,
        //assuming blocks is a partition of those states based on the output they give when symbol is applied.
        const auto is_valid = [N, &g](list<list<state>> const &blocks, input symbol, unordered_set<state> &valid_set)
        {
            for (auto &&block : blocks)
            {
                const auto new_blocks = partition_(begin(block), end(block), [symbol, &g](state state)
                {
                    return apply(g, state, symbol).to;
                }, N);
                for (auto &&new_block : new_blocks)
                {
                    if (new_block.size() != 1)
                    {
                        if (valid_set.empty())
                        {
                            return false;
                        } else
                        {
                            for (state s : new_block)
                            {
                                if (valid_set.count(s) > 0)
                                    return false;
                            }
                        }
                    }
                }
            }
            return true;
        };

        // We'll start with the root, obviously
        work_list.emplace_back(root, true);

        while (!work_list.empty())
        {
            splitting_tree &boom = work_list.front().work;
            unordered_set<state> valid_set = move(work_list.front().valid_states);
            bool split_on_output = work_list.front().split_on_output;
            work_list.pop_front();
            //the state for which the current tree prioritizes validity.
            //if there is no preference then the first one in the list is chosen.
            //used when selecting a separator from multiple options.
            //The preference is the one which is valid for valid_target, if it exists.
            state valid_target;
            if (!valid_set.empty())
                valid_target = *valid_set.begin();
            else
                valid_target = boom.states.front();

            const size_t depth = boom.depth;

            if (boom.states.size() == 1) continue;

            //possible progress is splitted into tree different categories: on output, on state and force_progress.
            //for each loop iteration, only one of these three categories is executed. the choise depends on the value of some flags.

            //first try to split on just output.
            //if a previous check on output failed, then doing it again serves no purpose so you can skip it.
            if (split_on_output)
            {
                // First try to split on output
                for (input symbol : all_inputs)
                {
                    const auto new_blocks = partition_(
                            begin(boom.states),
                            end(boom.states), [symbol, depth, &g](state state)
                            {
                                const auto r = apply(g, state, symbol);
                                return r.out;
                            }, Q);

                    // no split -> continue with other input symbols
                    if (new_blocks.size() == 1) continue;

                    // not a valid split -> continue
                    if (!is_valid(new_blocks, symbol, valid_set)) continue;

                    // a succesful split, update partition and add the children
                    boom.separators.emplace_back(word(1, symbol));
                    add_push_new_block(work_list, new_blocks, boom, valid_set, false);

                    goto has_split;
                }
                //no splits on output does not mean there are no splits on state,
                //so dont increase the days without progress timer until that check is also done.
                goto has_no_split;
            }

            //if we want to force progression, we will focus on making a valid split for each individual state, instead of for all of them at the same time.
            //this gives more possible splits, but also results in more work.
            if (force_progress)
            {
                //we only force progress when all other options have been exhausted.
                //the forced progress might open new options, for which we check first before we force progress again.
                force_progress = false;

                //the valid set should be either empty or a (possibly improper) subset of the states of this node.
                assert(valid_set.empty() || [&valid_set](const splitting_tree &boom)
                {
                    for (const state s : valid_set)
                    {
                        if (std::find(boom.states.begin(), boom.states.end(), s) != boom.states.end())
                        {
                            continue;
                        } else
                        {
                            return false;
                        }
                    }
                    return true;
                }(boom));

                //the set of all states for which we want to be valid: the intersection between valid_set and boom.states.
                unordered_set<state> targets;
                //an empty set actually means its valid for everything, but you cant delete states from an empty set.
                //So we explicitly fill it here.
                if (valid_set.empty())
                    targets = unordered_set<state>(boom.states.begin(), boom.states.end());
                else
                    targets = unordered_set<state>(valid_set);


                //a list of all the splits that were made.
                //they are saved in a list so that they can be modified if required before they are pushed onto the work_list.
                //each split is a tuple of three elements.
                //element 0 is the seperator. element 1 is the partion based on output, and element 2 is the valid_set.
                vector<tuple<word, list<list<state>>, unordered_set<state>>> splits;

                //in the case were we cant find the perfect solution, we record the next best thing.
                //the best general split is defined as the split which is valid for the most states.
                //if no split is valid for any state, which is possible, then we simply record the first possible split.
                word best_separator = {};       //separator of best general split so far.
                int best_separator_value = -1;  //amount of states for which the best general split so far is valid.
                list<list<state>> best_separator_blocks; // the partition of states based on best_separator.
                unordered_set<state> best_seperator_valid_set; //the set of states for which the best_separator is valid.
                int best_split_index = -1;    //index of best split in list of splits. might not exist

                for (input symbol : all_inputs)
                {
                    std::list<std::list<state>> new_blocks = partition_(
                            begin(boom.states),
                            end(boom.states), [symbol, depth, &g](state state)
                            {
                                const auto r = apply(g, state, symbol);
                                return r.out;
                            }, Q);

                    // no split -> continue with other input symbols
                    if (new_blocks.size() == 1) continue;


                    unordered_set<state> new_valid_set = is_valid_for(N, g, new_blocks, symbol);
                    bool updated_separator = false;
                    //compare current split with best general split so far. Update if necessary.
                    //extra check required because .size() returns a size_t which will cause the -1 to be converted to max_uint
                    if (best_separator_value == -1 || new_valid_set.size() > best_separator_value)
                    {
                        best_separator = {symbol};
                        best_separator_value = new_valid_set.size();
                        updated_separator = true;
                    }

                    // split is invalid for all the targets -> record info if required and continue
                    if (!update_valid_set(targets, new_valid_set))
                    {
                        if (updated_separator)
                        {
                            best_separator_blocks = move(new_blocks);
                            best_seperator_valid_set = move(new_valid_set);
                        }
                        continue;
                    }

                    assert(new_blocks.size() > 1);
                    //todo: possibly use the intersection of new_valid_set and valid_set instead of just new_valid_set
                    // a succesful split, save the required information for later.
                    splits.emplace_back(word(1, symbol), move(new_blocks), move(new_valid_set));
                    if (updated_separator)
                    {
                        best_split_index = splits.size() - 1;
                    }


                    //if a valid split has been found for all the targets the work is done and progression is made.
                    if (targets.empty())
                        break;
                }

                //if there are still some states for which we have failed to find a valid split on output, we will now look for valid splits on state.
                for (input symbol : all_inputs)
                {
                    if (targets.empty())
                        break;
                    else
                    {
                        valid_target = *targets.begin();
                    }

                    vector<bool> successor_states(N, false);
                    for (auto &&state : boom.states)
                    {
                        successor_states[apply(g, state, symbol).to] = true;
                    }
                    //the state to which the valid target moves when given the selected symbol as input.
                    state
                    valid_target_image = apply(g, valid_target, symbol).to;

                    const auto &oboom = lca(root, [&successor_states](state state) -> bool
                    {
                        return successor_states[state];
                    }, valid_target_image);

                    // a leaf, hence not a split -> try other symbols
                    if (oboom.children.empty()) continue;

                    // possibly a succesful split, construct the children
                    vector<input> word = concat(vector<input>(1, symbol), oboom.get_separator(valid_target_image));
                    list<list<state>> new_blocks = partition_(
                            begin(boom.states),
                            end(boom.states), [word, depth, &g](state state)
                            {
                                const mealy::edge r = apply(g, state, word.begin(), word.end());
                                return r.out;
                            }, Q);


                    unordered_set<state> new_valid_set = is_valid_for(N, g, new_blocks, symbol);
                    bool updated_separator = false;
                    if (best_separator_value == -1 || new_valid_set.size() > best_separator_value)
                    {
                        best_separator = word;
                        best_separator_value = new_valid_set.size();
                        updated_separator = true;
                    }

                    // split is invalid for all the targets -> record info if required and continue.
                    if (!update_valid_set(targets, new_valid_set))
                    {
                        if (updated_separator)
                        {
                            best_separator_blocks = move(new_blocks);
                            best_seperator_valid_set = move(new_valid_set);
                        }
                        continue;
                    }

                    assert(new_blocks.size() > 1);

                    // a succesful split, save the required information for later.
                    splits.emplace_back(move(word), move(new_blocks), move(new_valid_set));
                    if (updated_separator)
                    {
                        best_split_index = splits.size() - 1;
                    }

                }

                //if we get here, then we tried all the possible partialy valid splits.
                //there might however still be states left in targets for which there was no valid split.
                //for these states we use the recorded best general split so far instead.

                //if the best general split is already queued, update its valid_list to include the remaining targets.
                if (best_split_index != -1)
                {
                    for (state s : targets)
                    {
                        get<2>(splits[best_split_index]).emplace(s);
                    }

                } else if (best_separator.empty())
                {
                    //if we get here then there was not a single input that could split any of the states from the others on either output state or output symbol.
                    //there are only two possible ways that this can happen. 1: the two states are equal, or 2: they could be split on output state but the work needed to
                    //determine the required separator is still in the work_list.
                    //option 1 is excluded because the specification is reduced and the names of the states differ.
                    //to resolve the problem we postpone this split and try to force progress elsewhere first.
                    force_progress = true;
                    days_without_progress++;
                    //make sure we wont endlessly run in circles.
                    //the +1 is to account for the current boom that is not in the work list right now but will be put back later.
                    if (days_without_progress >= 3 * (work_list.size() + 1))
                    {
                        //if we get here then the algorithm has failed.
                        //we return the root anyway so we can inspect the partial solution in the output file.
                        cout << "warning, algorithm failed. returning partial results\n";
                        return root;
                    }
                    goto has_no_split;

                } else
                {
                    //if the best general split is not already queued, then queue it now using the stored info.
                    for (state s : targets)
                    {
                        best_seperator_valid_set.emplace(s);
                    }
                    assert(best_separator_blocks.size() > 1);
                    splits.emplace_back(move(best_separator), move(best_separator_blocks),
                                        move(best_seperator_valid_set));
                }

                //push all the defined splits into the work_list now.
                for (auto split : splits)
                {

                    boom.separators.push_back(move(get<0>(split)));
                    add_push_new_block(work_list, get<1>(split), boom, get<2>(split), true);

                }
                goto has_split;

            }


            // execution only gets here when split_on_output is false. This means we need to check for splits on state.
            for (input symbol : all_inputs)
            {
                vector<bool> successor_states(N, false);
                for (auto &&state : boom.states)
                {
                    successor_states[apply(g, state, symbol).to] = true;
                }
                //the state to which the valid target moves when given the selected symbol as input.
                state
                valid_target_image = apply(g, valid_target, symbol).to;

                const auto &oboom = lca(root, [&successor_states](state state) -> bool
                {
                    return successor_states[state];
                }, valid_target_image);

                // a leaf, hence not a split -> try other symbols
                if (oboom.children.empty()) continue;

                // possibly a succesful split, construct the children
                const vector<input> word = concat(vector<input>(1, symbol), oboom.get_separator(valid_target_image));
                const auto new_blocks = partition_(
                        begin(boom.states),
                        end(boom.states), [word, depth, &g](state state)
                        {
                            const mealy::edge r = apply(g, state, word.begin(), word.end());
                            return r.out;
                        }, Q);

                // not a valid split -> continue
                if (!is_valid(new_blocks, symbol, valid_set)) continue;

                assert(new_blocks.size() > 1);

                // update partition and add the children
                boom.separators.push_back(word);
                add_push_new_block(work_list, new_blocks, boom, valid_set, false);

                goto has_split;

            }


            //has every element in work_list been checked without succes?
            if (days_without_progress++ >= work_list.size())
            {
                // there is not a single valid split that can stil be made, so we have to force progress by making an invalid split.
                force_progress = true;
            }

            has_no_split:
            //no valid splits on the current work_set. put it back and try the next one.
            work_list.emplace_back(boom, move(valid_set), false);

            continue;

            has_split:
            days_without_progress = 0;
        }

        return root;
    }
}
//
// Created by Gijs van Cuyck on 19/12/2018.
//

#ifndef COMPLETE_ADS_SPLIT_TREE_ANALYSYS_HPP
#define COMPLETE_ADS_SPLIT_TREE_ANALYSYS_HPP
#include "../hybrid_lib/splitting_tree.hpp"

#include <algorithm>

namespace analysis
{
    int count_leaves(const hybrid::splitting_tree & tree)
    {
        if(tree.children.empty())
            return 1;
        else
        {
            int counter  = 0;
            for(const hybrid::splitting_tree & child: tree.children)
            {
                counter += count_leaves(child);
            }
            return counter;
        }
    }

    double average_uncertainty(const hybrid::splitting_tree & tree)
    {
        return static_cast<double> (tree.states.size())/count_leaves(tree);
    }

    int maximum_uncertainty(const hybrid::splitting_tree & tree)
    {
        if(tree.children.empty())
        {
            return tree.states.size();
        } else
        {
            int max = 0;
            for(const hybrid::splitting_tree & child: tree.children)
            {
                max = std::max(max,maximum_uncertainty(child));
            }
            return max;
        }
    }


}

#endif //COMPLETE_ADS_SPLIT_TREE_ANALYSYS_HPP

//
// Created by Gijs van Cuyck on 20/11/2018.
//

#include "test_suite.hpp"
#include "trie.hpp"
#include "vector_printing.hpp"
#include <numeric>
#include <queue>
#include <cassert>
#include <iostream>
namespace complete
{
//creates a minimal state cover for the given mealy machine using breath first search
    state_cover create_state_cover(const mealy &machine, const state starting_state)
    {
        std::vector<bool> added(machine.graph_size, false);
        state_cover cover(machine.graph_size);
        std::vector<input> all_inputs(machine.input_size);
        iota(begin(all_inputs), end(all_inputs), input(0));

        std::queue<state> work;
        work.push(starting_state);
        added[starting_state] = true;
        while (!work.empty())
        {
            const state s = work.front();
            work.pop();

            for (input i : all_inputs)
            {
                const auto v = apply(machine, s, i).to;
                if (added[v]) continue;

                work.push(v);
                added[v] = true;
                cover[v] = cover[s];
                cover[v].push_back(i);
            }
        }
        assert(machine.graph_size == cover.size());
        return cover;
    }


//stores all the combinations of an element from prefixes concatinated with an element from suffixes in the output_buffer.
    template<typename Fun>
    void set_concatination(const std::vector<word> &prefixes, const std::vector<word> &suffixes, Fun &&output_buffer)
    {
        for (const word &x : prefixes)
        {
            for (const word &y : suffixes)
            {
                word ret;
                ret.reserve(x.size() + y.size());
                ret.insert(ret.begin(), x.begin(), x.end());
                ret.insert(ret.end(), y.begin(), y.end());
                output_buffer(ret);
            }
        }

    }

    test_suite create_test_suite(const separating_family &family, const mealy &m, const state starting_state)
    {
        //storing the intermediate results in a trie structure removes all sequences that are prefixes of a different sequence.
        //this prevents unnecessary double testing.
        trie<input> TS;
        //lambda function that can be passed along to other functions that will buffer all its calls in the test_suite
        auto TS_input = [&TS](const word &sequence)
        {
            TS.insert(sequence);
        };
        const state_cover s_cover = create_state_cover(m, starting_state);

        //first part of Wp method: checking if all the states exist by going to every state, and then applying every state identifier in every state.
        for (const separating_set &state_identifier : family)
        {
            set_concatination(s_cover, state_identifier, TS_input);
        }

        //second part of Wp method: checking if all the transitions exist by going to every state,
        //then executing every possible input, and then using the state identifier for the state you end up in.
        for (state s = 0; s < m.graph_size; s++)
        {
            for (input i = 0; i < m.input_size; i++)
            {
                state expected_output_state = apply(m, s, i).to;
                for (const word &identifier:family[expected_output_state])
                {
                    word buffer;
                    buffer.reserve(s_cover[s].size() + 1 + identifier.size());
                    buffer.insert(buffer.end(), s_cover[s].begin(), s_cover[s].end());
                    buffer.push_back(i);
                    buffer.insert(buffer.end(), identifier.begin(), identifier.end());
                    TS.insert(buffer);
                }
            }

        }

        return flatten(TS);
    }

    void test_suite_to_stream(std::ostream &output, const test_suite &TS, const std::vector<std::string> &inputs)
    {
        for (const word &test:TS)
        {
            std::vector<std::string> translated_word(test.size());
            transform(test.begin(), test.end(), translated_word.begin(), [&inputs](size_t symbol_index)
            { return inputs[symbol_index]; });
            output << join(translated_word.begin(), translated_word.end(), ",", "\n");

        }
    }
}
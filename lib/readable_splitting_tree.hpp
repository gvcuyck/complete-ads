//
// Created by Gijs van Cuyck on 19/10/2018.
//

#ifndef COMPLETE_ADS_READABLE_SPLITTING_TREE_HPP
#define COMPLETE_ADS_READABLE_SPLITTING_TREE_HPP

#include "splitting_tree.hpp"

namespace complete
{
//a regular splitting tree, but with all the state and input indexes replaced by their actual string values.
    struct readable_splitting_tree
    {
        /// \brief constructor that presets the internal vectors to the correct sizes for filling with iterators.
        readable_splitting_tree(size_t state_amount, size_t depth_value)
                : states(state_amount), depth(depth_value)
        {}

        /// \brief constructor used to initialize the list of children with empty readable splitting trees.
        readable_splitting_tree() = default;

        std::vector<std::string> states;
        std::vector<std::vector<readable_splitting_tree>> children;
        std::vector<std::vector<std::string>> separators;
        std::unordered_map<std::string, size_t> valid_map;
        size_t depth = 0;

    };

//uses the translations described in inputs and outputs to translate a splitting tree into a readable splitting tree.
    readable_splitting_tree translate_splitting_tree(const splitting_tree &tree, const std::vector<std::string> &inputs,
                                                     const std::vector<std::string> &states);

//outputs a readable splitting tree into an ostream. used for turning it into a string or saving it to a file.
    std::ostream &rst_to_stream(readable_splitting_tree &tree, std::ostream &output);

}
#endif //COMPLETE_ADS_READABLE_SPLITTING_TREE_HPP

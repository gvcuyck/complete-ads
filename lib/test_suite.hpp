//
// Created by Gijs van Cuyck on 20/11/2018.
//
#pragma once

#include "types.hpp"
#include "separating_family.hpp"
#include <vector>
namespace complete
{
//some new names to make the code more readable.
    using state_cover = std::vector<word>;
    using test_suite = std::vector<word>;

//creates a test_suite using the Wp method, using family as state identifiers.
    test_suite create_test_suite(const separating_family &family, const mealy &m, const state starting_state);

    void test_suite_to_stream(std::ostream &output, const test_suite &TS, const std::vector<std::string> &inputs);


}
//
// Created by gijsc on 2-1-2019.
//

#ifndef COMPLETE_ADS_RESULTS_ANALYSIS_H
#define COMPLETE_ADS_RESULTS_ANALYSIS_H

#include <string>

namespace analysis{
    void analyze_results(std::string input_filename,std::string output_directory);
}
#endif //COMPLETE_ADS_RESULTS_ANALYSIS_H

//
// Created by Gijs van Cuyck on 18/10/2018.
//

#ifndef COMPLETE_ADS_PRINTING_H
#define COMPLETE_ADS_PRINTING_H

#include <string>
#include <sstream>
#include <vector>
#include <list>

namespace complete
{
/// \brief turns a vector of printable objects into a string, putting a sepperator between every two elements and
/// a concluder after the last one.
///implementation taken from: https://codereview.stackexchange.com/questions/142902/simple-string-joiner-in-modern-c
    template<typename InputIt>
    std::string join(InputIt begin,
                     InputIt end,
                     const std::string &separator = "",
                     const std::string &concluder = "")
    {
        std::ostringstream ss;

        if (begin != end)
        {
            ss << *begin++;
        }

        while (begin != end)
        {
            ss << separator;
            ss << *begin++;
        }

        ss << concluder;
        return ss.str();
    }

    template<typename T>
    std::ostream &operator<<(std::ostream &s, std::vector<T> v)
    {
        for (int i = 0; i < v.size(); i++)
        {
            s << i << " = " << v[i] << "\n";
        }
        return s;
    }

//turns a list into a vector.
//used for debugging, since lists are not very human readable and vectors are.
    template<typename T>
    std::vector<T> list_to_vector(const std::list<T> &input)
    {
        std::vector<T> ret;
        ret.reserve(input.size());
        std::copy(std::begin(input), std::end(input), std::back_inserter(ret));
        return ret;
    }

//turns a list of lists into a vector of vectors.
//used for debugging, since lists are not very human readable and vectors are.
    template<typename T>
    std::vector<std::vector<T>> double_list_to_vector(const std::list<std::list<T>> &input)
    {

        std::vector<std::vector<T>> ret;
        for (std::list<T> element: input)
        {
            ret.push_back(list_to_vector(element));
        }
        return ret;
    }
}

#endif //COMPLETE_ADS_PRINTING_H

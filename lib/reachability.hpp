#pragma once

#include "types.hpp"

namespace complete
{
    struct mealy;

    mealy reachable_submachine(const mealy &in, std::uint16_t start);
}
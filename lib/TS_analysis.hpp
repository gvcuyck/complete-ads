//
// Created by Gijs van Cuyck on 28/11/2018.
// code related to analysing test suites
//

#ifndef COMPLETE_ADS_TS_ANALYSYS_HPP
#define COMPLETE_ADS_TS_ANALYSYS_HPP

#include <vector>
#include <string>
namespace analysis{

    //calculate the total number of symbols in the test suite where the cost of a reset can be chosen manually.
    template<typename T>
    int calculate_TS_lenght(const std::vector<std::vector<T>> & TS, int reset_cost = 1)
    {
        int result = 0;
        for(const std::vector<T> & test : TS)
        {
            result+=test.size()+reset_cost;
        }
        return result;

    }

}

#endif //COMPLETE_ADS_TS_ANALYSYS_HPP

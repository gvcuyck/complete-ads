//complete ads code
#include "../lib/mealy.hpp"
#include "../lib/reachability.hpp"
#include "../lib/read_mealy.hpp"
#include "../lib/splitting_tree.hpp"
#include "../lib/readable_splitting_tree.hpp"
#include "../lib/vector_printing.hpp"
#include "../lib/separating_family.hpp"
#include "../lib/test_suite.hpp"

//hybrid ads code
#include "../hybrid_lib/splitting_tree.hpp"
#include "../hybrid_lib/read_mealy.hpp"
#include "../hybrid_lib/mealy.hpp"
#include "../hybrid_lib/reachability.hpp"
#include "../hybrid_lib/adaptive_distinguishing_sequence.hpp"
#include "../hybrid_lib/transfer_sequences.hpp"
#include "../hybrid_lib/separating_family.hpp"
#include "../hybrid_lib/trie.hpp"
#include "../hybrid_lib/test_suite.hpp"

//analysis code
#include "../lib/TS_analysis.hpp"
#include "../lib/splitting_tree_analysis.hpp"
#include "../lib/results_analysis.hpp"

//stdlib code
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <random>
#include <stdexcept>
#include <string>
#include <utility>
#include <functional>
#include <cassert>


#ifdef _WIN32
extern "C" {
#include "../lib/windows_getopt.h"
}
#else
#include <unistd.h>
#endif

static const char USAGE[] =
        R"(Generate or stream a test suite for a given FSM.

    Usage:
      main [options]

    Options:
      -h             Show this screen
      -t             print the test suites in the output file. (grow very fast with large input)
      -j             only run the input through the hybrid-ads method. used mainly for testing
      -d             overwrite old comparison results file contents instead of appending.
      -c             compare the resulting test suite with the results given by the hybrid ads code.
      -a             treat the input file name as an absolute path instead of a relative one.
      -r <filename>  analyze the compare result file and print some statistics.
      -C <filename>  the file in which to store compare results. dont sepcify for default filename.
                     The path should be relative to the outputs/compare_results directory.
                     This value is only used if the -c options is given, otherwise it is ignored.
      -k <num>       Number of extra states to check for (minus 1). currently not supported
      -x <seed>      32 bits seeds for deterministic execution (0 is not valid). currently not supported
      -f <filename>  Input filename ('-' or don't specify for default example). path should be relative to examples directory
      -O <directory> Output directory (don't specify for default outputs directory). changing this will not change where the compare results are stored.
                     This argument should be given before the output filename, otherwise its value is ignored.
      -o <filename>  Output filename ('-' or don't specify for default output file). path should be relative to output directory

)";

std::string output_directory = "../outputs/";
std::string input_directory = "../examples/";
const std::string compare_directory = output_directory + "compare_results/";

//extra datatypes for the hybrid code



struct main_options
{
    bool help = false;
    bool do_compare = false;
    bool reset_compare_file = false;
    bool print_test_suites = false;
    bool only_hybrid = false;
    bool do_compare_file_analysis = false;

    //"coffe_machine.dot";
    //"lee_yannakakis_difficult.dot";
    //"lee_yannakakis_distinguishable.dot";
    //"esm-manual-controller.dot";
    //"ABP_Sender.flat_0_1.dot";
    //"ex5_with_loops_with_hidden_states_minimized.dot";
    //ram_test_minimized.dot
    //"../benchmarks/principle/BenchmarkCircuits/ram_test_minimized.dot";
    std::string input_filename =
            input_directory + "train4_with_loops_with_hidden_states_minimized.dot";
    std::string output_filename = "";
    std::string hybrid_output_filename = "";
    std::string compare_filename = compare_directory + "compare_results.txt";

    //extra options for the hybrid code
    unsigned long k_max = 1;      // 3 means 2 extra states
    //todo: ask joshua what this does
    unsigned long l = 0;          // length 0, 1 will be redundancy free
    unsigned long rnd_length = 0; // in addition to k_max
    unsigned long seed = 0;       // 0 for unset/noise
};

main_options parse_options(int argc, char **argv)
{
    main_options opts;


    try
    {
        int c;
        while ((c = complete::getopt(argc, argv, "htjdcar:C:k:x:f:O:o:")) != -1)
        {
            switch (c)
            {
                case 'h': // show help message
                    opts.help = true;
                    break;
                case 't': //print test suites
                    opts.print_test_suites = true;
                    break;
                case 'j': //run just the hybrid code
                    opts.only_hybrid = true;
                    break;
                case 'd': //overwrite old compare file contents.
                    opts.reset_compare_file = true;
                    break;
                case 'c': //compare results with hybrid code
                    opts.do_compare = true;
                    break;
                case 'a': //use absolute input paths
                    input_directory = "";
                    break;
                case 'r': //analyze compare results file
                    opts.do_compare_file_analysis = true;
                    opts.compare_filename = compare_directory + complete::optarg;
                    break;
                case 'C': //change compare results file
                    opts.compare_filename = compare_directory + complete::optarg;
                    break;
                case 'k': // select extra states / k-value
                    opts.k_max = std::stoul(complete::optarg);
                    break;
                case 'x': // seed
                    opts.seed = std::stoul(complete::optarg);
                    break;
                case 'f': // input filename
                    opts.input_filename = input_directory + complete::optarg;
                    break;
                case 'O': //output directory
                    output_directory = complete::optarg;
                    break;
                case 'o': // output filename
                    opts.output_filename = output_directory + complete::optarg;
                    opts.hybrid_output_filename = output_directory + "hybrid_" + complete::optarg;
                    break;
                case ':': // some option without argument
                    throw std::runtime_error(std::string("No argument given to option -") + char(complete::optopt));
                case '?': // all unrecognised things
                default: //should never happen but included for completeness sake.
                    throw std::runtime_error(std::string("Unrecognised option -") + char(complete::optopt));
            }
        }
    } catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << "Could not parse command line options." << std::endl;
        std::cerr << "Please use -h to see the available options." << std::endl;
        exit(2);
    }

    //generate outputfile name based on inputfile name.
    if (opts.output_filename.empty())
    {
        size_t start = opts.input_filename.find_last_of('/');
        if (start == std::string::npos)
            start = opts.input_filename.find_last_of('\\');
        size_t end = opts.input_filename.find_last_of('.');
        if (start >= end || end == std::string::npos)
        {
            opts.output_filename = output_directory + "default_output.txt";
            opts.hybrid_output_filename = output_directory + "default_hybrid_output.txt";
        } else
        {
            opts.output_filename = output_directory + opts.input_filename.substr(start, end - start) + "_output.txt";
            opts.hybrid_output_filename =
                    output_directory + opts.input_filename.substr(start, end - start) + "_hybrid_output.txt";
        }
    }

    return opts;
}


int main(int argc, char *argv[])
{
    /*
     * First we parse the command line options.
     * We quit when asked for help.
     */
    const auto args = parse_options(argc, argv);

    if (args.help)
    {
        std::cout << USAGE << std::endl;
        exit(0);
    }

    if (args.do_compare_file_analysis)
    {
        analysis::analyze_results(args.compare_filename, compare_directory);
        exit(0);
    }


    /*
     * Then all the setup is done. Parsing the automaton,
     * constructing all types of sequences needed for the
     * test suite.
     */
    auto machine_and_translation = [&]
    {
        const auto &filename = args.input_filename;
        if (filename.empty() || filename == "-")
        {
            return complete::read_mealy_from_dot(std::cin);
        }
        if (filename.find(".txt") != std::string::npos)
        {
            auto m = complete::read_mealy_from_txt(filename);
            auto t = create_translation_for_mealy(m);
            return std::make_pair(std::move(m), std::move(t));
        } else if (filename.find(".dot") != std::string::npos)
        {
            return complete::read_mealy_from_dot(filename);
        }

        std::clog << "warning: unrecognized file format, assuming .dot\n";
        return complete::read_mealy_from_dot(filename);
    }();

    //leftover old code. main purpose now is to check if machine if fully defined.
    const auto &reachable_machine = reachable_submachine(machine_and_translation.first, 0U);

    const auto &machine = machine_and_translation.first;

    const auto &translation = machine_and_translation.second;


    std::vector<std::string> input_translation = complete::create_reverse_map(translation.input_indices);
    std::vector<std::string> state_translation = complete::create_reverse_map(translation.state_indices);

    //only useful for debugging
    bool print_translation_help = false;
    if (print_translation_help)
    {
        using namespace complete;
        std::ofstream translation_file(output_directory + "translations.txt");
        translation_file << "input translation\n";
        translation_file << input_translation;
        translation_file << "\noutput translation\n";
        translation_file << create_reverse_map(translation.output_indices);
        translation_file << "\nstate translation\n";
        translation_file << state_translation;
        translation_file.close();
    }

    int complete_ts_lenght;
    int complete_ts_resets;
    if (args.do_compare || !args.only_hybrid)
    {
        std::cout << "generating complete style splitting tree\n";
        complete::splitting_tree complete_splitting_tree = create_splitting_tree(machine);

        complete::readable_splitting_tree translated_tree = translate_splitting_tree(complete_splitting_tree,
                                                                                     input_translation,
                                                                                     state_translation);
        std::ofstream out_file(args.output_filename);
        out_file << "the splitting tree:\n\n";
        rst_to_stream(translated_tree, out_file) << "\n\n\n";

        std::cout << "generating complete style separating family\n";
        complete::separating_family family = create_separating_family(complete_splitting_tree, machine);

        //making sure the results are actually correct.
        assert(test_separating_family(family, machine));


        out_file << "the separating family:\n\n";

        complete::separating_family_to_stream(out_file, family, input_translation, state_translation);


        std::cout << "generating Wp style test suite\n";
        complete::test_suite TS = create_test_suite(family, machine, complete::state(0));

        if (args.print_test_suites)
        {
            out_file << "the test suite:\n\n";
            complete::test_suite_to_stream(out_file, TS, input_translation);
        }
        if (args.do_compare)
        {
            complete_ts_lenght = analysis::calculate_TS_lenght(TS);
            complete_ts_resets = TS.size();
        }
        out_file.close();
    }


    if (args.do_compare || args.only_hybrid)
    {


        //create a test suite using the hybrid ads code as well to compare with.
        //starts from scratch to avoid the previous calculations from interfering.

        //start of original hybrid test suite code
        const auto machine_and_translation = [&]
        {
            const auto &filename = args.input_filename;
            if (filename == "" || filename == "-")
            {
                return hybrid::read_mealy_from_dot(std::cin);
            }
            if (filename.find(".txt") != std::string::npos)
            {
                const hybrid::mealy m = hybrid::read_mealy_from_txt(filename);
                const auto t = create_translation_for_mealy(m);
                return std::make_pair(std::move(m), std::move(t));
            } else if (filename.find(".dot") != std::string::npos)
            {
                return hybrid::read_mealy_from_dot(filename);
            }

            std::clog << "warning: unrecognized file format, assuming .dot\n";
            return hybrid::read_mealy_from_dot(filename);
        }();

        const auto &machine = hybrid::reachable_submachine(std::move(machine_and_translation.first), 0);
        const auto &translation = machine_and_translation.second;

        const auto random_seeds = [&]
        {
            std::vector<uint_fast32_t> seeds(4);
            if (args.seed != 0)
            {
                std::seed_seq s{args.seed};
                s.generate(seeds.begin(), seeds.end());
            } else
            {
                std::random_device rd;
                generate(seeds.begin(), seeds.end(), ref(rd));
            }
            return seeds;
        }();

        std::cout << "generating hopcroft style splitting tree\n";
        auto all_pair_separating_sequences = [&]
        {

            const auto splitting_tree_hopcroft = [&]
            {
                return hybrid::create_splitting_tree(
                        machine, hybrid::hopcroft_style,
                        random_seeds[0]);
            }();

            return splitting_tree_hopcroft.root;
        }();


        hybrid::splitting_tree hybrid_splitting_tree(0, 0);
        auto sequence = [&]
        {

            const auto tree = [&]
            {
                std::cout << "generating hybrid style splitting tree\n";
                auto result = create_splitting_tree(machine, hybrid::lee_yannakakis_style, random_seeds[1]);
                hybrid_splitting_tree = result.root;
                return result;

            }();

            const auto sequence_ = [&]
            {
                std::cout << "generating adaptive distinguising sequence\n";
                return hybrid::create_adaptive_distinguishing_sequence(tree);
            }();

            return sequence_;
        }();

        auto transfer_sequences = create_transfer_sequences(hybrid::minimal_transfer_sequences, machine, 0,
                                                            random_seeds[2]);

        auto const inputs = hybrid::create_reverse_map(translation.input_indices);

        std::cout << "generating hybrid style separating family\n";
        const auto separating_family = hybrid::create_separating_family(sequence, all_pair_separating_sequences);

        hybrid::trie <hybrid::input> test_suite;
        hybrid::word buffer;
        const auto output_word = [&inputs](const auto &w)
        {
            for (const auto &x : w)
            {
                std::cout << inputs[x] << ' ';
            }
            std::cout << std::endl;
        };
        std::cout << "generating hybrid style test suite\n";
        std::vector<hybrid::word> mid_sequences(1);
        hybrid::test(machine, transfer_sequences, mid_sequences, separating_family, args.k_max + 1,
                     {[&buffer](auto const &w)
                      { buffer.insert(buffer.end(), w.begin(), w.end()); },
                      [&buffer, &test_suite]()
                      {
                          test_suite.insert(buffer);
                          buffer.clear();
                          return true;
                      }});

        std::vector<hybrid::word> hybrid_test_suite = flatten(test_suite);

        //end of original hybrid test suite code.

        if (args.print_test_suites)
        {
            std::ofstream hybrid_out_file(args.hybrid_output_filename);
            hybrid_out_file << "the test suite:\n\n";
            for (const hybrid::word &test : hybrid_test_suite)
            {
                std::vector<std::string> translated_word(test.size());
                transform(test.begin(), test.end(), translated_word.begin(), [&inputs](size_t symbol_index)
                { return inputs[symbol_index]; });
                hybrid_out_file << complete::join(translated_word.begin(), translated_word.end(), ",", "\n");
            }
            hybrid_out_file.close();
        }

        if (args.do_compare)
        {
            std::ofstream result_stats_file;
            if (args.reset_compare_file)
                result_stats_file = std::ofstream(args.compare_filename);
            else
                result_stats_file = std::ofstream(args.compare_filename, std::ios_base::app);
            size_t start = args.input_filename.find_last_of('/');
            if (start == std::string::npos)
                start = args.input_filename.find_last_of('\\');
            std::string filename = args.input_filename.substr(start);

            const double average_uncertainty = analysis::average_uncertainty(hybrid_splitting_tree);
            const double max_uncertainty = analysis::maximum_uncertainty(hybrid_splitting_tree);
            int hybrid_ts_lenght = analysis::calculate_TS_lenght(hybrid_test_suite);
            result_stats_file << "results for " << filename << "\n";
            result_stats_file << "input size:\n";
            result_stats_file << "\tstates: " << machine.graph_size << "\n";
            result_stats_file << "\tinputs: " << machine.input_size << "\n";
            result_stats_file << "ADS aplicability:\n";
            result_stats_file << "\taverage uncertainty: " << average_uncertainty << "\n";
            result_stats_file << "\tmaximum uncertainty: " << max_uncertainty << "\n";
            result_stats_file << "\taverage uncertainty gain: " << 100 * (static_cast<double>(machine.graph_size -
                                                                                              average_uncertainty) /
                                                                          machine.graph_size) << "%\n";
            result_stats_file << "\tmaximum uncertainty gain: "
                              << 100 * (static_cast<double>(machine.graph_size - max_uncertainty) / machine.graph_size)
                              << "%\n";
            result_stats_file << "complete test suite:\n";
            result_stats_file << "\ttotal length: " << complete_ts_lenght << "\n";
            result_stats_file << "\tresets: " << complete_ts_resets << "\n";
            result_stats_file << "hybrid test suite:\n";
            result_stats_file << "\ttotal length: " << hybrid_ts_lenght << "\n";
            result_stats_file << "\tresets: " << hybrid_test_suite.size() << "\n";
            result_stats_file << "complete test suite improvements: (negative numbers represent worse results)\n";
            result_stats_file << "\tpercentage lenth improvement: "
                              << 100.0 * (hybrid_ts_lenght - complete_ts_lenght) / (hybrid_ts_lenght) << "%\n";
            result_stats_file << "\tpercentage resets improvement: "
                              << 100.0 * (int) (hybrid_test_suite.size() - complete_ts_resets) /
                                 (hybrid_test_suite.size())
                              << "%\n\n";
            result_stats_file.close();

        }
    }
    std::cout << "finished!\n";
    exit(0);


}
